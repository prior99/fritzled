# FritzLED

ESP32 Firmware for controlling SK6812-based light installations in fritz-kola boxes.

## Prerequirements

In order to build this project, some of the basic steps of [installing esp-idf](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/index.html) need to be taken:

### 1. Install esp-idf

Clone [esp-idf](https://github.com/espressif/esp-idf) somewhere (**recursively**). It's recommended to do a user-based installation rather than a system-wide installation.

After cloning the repository, it needs to install some dependencies: Execute the `install.sh` script inside. It will download the toolchains among other things into your home directory into a directory called `.espressif`. Do **not** execute it with `sudo`.

```
git clone --recursive https://github.com/espressif/esp-idf ~/.local/esp-idf
~/.local/esp-idf/install.sh
```

Do not execute `export.sh`, it's not required.

### 2. Configure your shell

You'll need to make some directories available ton your `$PATH` as well as set `$IDF_PATH`.
Put these lines into your `.bashrc` (or whatever that file is named for your shell):

```
export PATH=~/.local/esp-idf/tools/:$PATH
export PATH=~/.espressif/tools/xtensa-esp32-elf/esp32-2019r1-8.2.0/xtensa-esp32-elf/bin:$PATH
export IDF_PATH=~/.local/esp-idf
```

### 3. Dependencies

Some utilities will be used. Make sure you have a propert C/C++ Toolchain available, and install `make`, `cmake`, `ninja` and `virtualenv`.

In order to run the demo, you will also need to have `SDL2` and `SDL2_gfx` installed.

## Building

There are two targets available in this project:

1. `fritzled-demo`, a SDL2-based "emulation" of the FritzLED light-installation.
2. `fritzled-firmware`, the ESP32-based firmware.

### Building the Demo

```
make demo
```

### Building the Firmware

```
make firmware
```

### Working with the demo

Execute the demo using the `Makefile`.

```
make run
```

### Working with the firmware

You can flash the firmware and monitor the output like so:

```
TTY=/dev/ttyUSB0 make flash
TTY=/dev/ttyUSB0 make monitor
```

### Hardware Setup

* Connect a pull-up button on GPIO 25.
* Connect a pull-up button on GPIO 26.
* Connect a pull-up button on GPIO3,507.
* Connect a status LED on GPIO 22.
* Connect GPIO 18 to a logic-level converter converting the 3.3V voltage to the 5V provided by the ESP32. Then connect the 5V output pin to the DIN pin of the first SK6812 chip.
* Connect GND and VCC of SK6812 with the ESP32's GND and 5V power supply.

### Watch

Use `watchexec` to watch the build.

```
watchexec --watch components/ --restart 'make run & move container to workspace 4'
```

If you're using `i3`, you can stick the window to one workspace: ` assign [title="FritzLED Demo"] 4`.

## Hardware Layout

Layout the bottles inside a box so that the shortest possible connection to the next bottle is made:

![](images/bottles-order.png)

Layout multiple boxes in the same system:

![](images/boxes-order.png)