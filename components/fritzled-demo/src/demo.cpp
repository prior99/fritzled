#include "demo.h"
#include <SDL2/SDL2_gfxPrimitives.h>
#include <cmath>
#include <iostream>
#include <scenes.h>
#include <memory>

Demo::Demo(Setup setup, uint32_t width, uint32_t height)
    : setup(setup),
      grid(Grid(&this->setup)),
      scene(scene_by_index(scene_index, &grid)),
      window_width(width),
      window_height(height),
      last_frame_time(0) {
}

Demo::~Demo() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

int Demo::init() {
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
        std::cerr << "SDL_Init Error: " << SDL_GetError() << std::endl;
        return 1;
    }

    window = SDL_CreateWindow("FritzLED Demo", 100, 100, width(), height(), SDL_WINDOW_SHOWN);
    if (window == nullptr) {
        std::cerr << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
        SDL_Quit();
        return 1;
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == nullptr) {
        SDL_DestroyWindow(window);
        std::cerr << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
        SDL_Quit();
        return 1;
    }
    SDL_RenderSetLogicalSize(renderer, width(), height());

    return 0;
}

uint32_t Demo::bottle_width() {
    return (box_width() - box_padding_x() * 2) / setup.box_width;
}

uint32_t Demo::bottle_height() {
    return (box_height() - box_padding_y() * 2) / setup.box_height;
}

uint32_t Demo::physical_height() {
    return PHYSICAL_BOX_HEIGHT_OUTER_MM * setup.boxes_y;
}

uint32_t Demo::physical_width() {
    return PHYSICAL_BOX_WIDTH_OUTER_MM * setup.boxes_x;
}

uint32_t Demo::box_height() {
    return height() / setup.boxes_y;
}

uint32_t Demo::box_width() {
    return width() / setup.boxes_x;
}

uint32_t Demo::box_padding_x() {
    auto padding_ratio =
        (PHYSICAL_BOX_WIDTH_OUTER_MM - PHYSICAL_BOX_WIDTH_INNER_MM) / (float)PHYSICAL_BOX_WIDTH_OUTER_MM;
    return box_width() * padding_ratio / 2 + 2;
}

uint32_t Demo::box_padding_y() {
    auto padding_ratio =
        (PHYSICAL_BOX_HEIGHT_OUTER_MM - PHYSICAL_BOX_HEIGHT_INNER_MM) / (float)PHYSICAL_BOX_HEIGHT_OUTER_MM;
    return box_height() * padding_ratio / 2 + 2;
}

uint32_t Demo::width() {
    if (window_width / (float)physical_width() > window_height / (float)physical_height()) {
        return (uint32_t)(window_height * (physical_width() / (float)physical_height()));
    } else {
        return window_width;
    }
}

uint32_t Demo::height() {
    if (window_height / (float)physical_height() > window_width / (float)physical_width()) {
        return (uint32_t)(window_width * (physical_height() / (float)physical_width()));
    } else {
        return window_height;
    }
}

void Demo::draw_bottle(uint32_t box_x_index, uint32_t box_y_index, uint32_t bottle_x_index, uint32_t bottle_y_index) {
    auto box_x_offset = box_width() * box_x_index + box_padding_x();
    auto box_y_offset = box_height() * box_y_index + box_padding_y();
    auto bottle_x_offset = box_x_offset + bottle_width() * bottle_x_index;
    auto bottle_y_offset = box_y_offset + bottle_height() * bottle_y_index;
    auto center_x = bottle_x_offset + bottle_width() / 2;
    auto center_y = bottle_y_offset + bottle_height() / 2;
    auto radius = std::min(bottle_height(), bottle_width()) / 2;
    auto color = grid.get_box(box_x_index, box_y_index)->get_color(bottle_x_index, bottle_y_index)->get_rgb();
    filledCircleRGBA(renderer, center_x, center_y, radius, color.r, color.g, color.b, 255);
}

void Demo::draw_box(uint32_t x_index, uint32_t y_index) {
    auto x_offset = box_width() * x_index;
    auto y_offset = box_height() * y_index;
    boxRGBA(
        renderer,
        x_offset + 2,
        y_offset + 2,
        x_offset + box_width() - 2,
        y_offset + box_height() - 2,
        32,
        32,
        32,
        255);
    boxRGBA(
        renderer,
        x_offset + box_padding_x(),
        y_offset + box_padding_y(),
        x_offset + box_width() - box_padding_x(),
        y_offset + box_height() - box_padding_y(),
        8,
        8,
        8,
        255);
    for (uint32_t x = 0; x < setup.box_width; ++x) {
        for (uint32_t y = 0; y < setup.box_height; ++y) {
            draw_bottle(x_index, y_index, x, y);
        }
    }
    for (uint32_t y = 1; y < 4; ++y) {
        auto box_y_offset = box_height() * y_index + box_padding_y();
        auto bottle_y_offset = box_y_offset + bottle_height() * y;
        auto center_y = bottle_y_offset;
        boxRGBA(
            renderer,
            x_offset + 2,
            center_y - 2,
            x_offset + box_width() - 2,
            center_y + 2,
            32,
            32,
            32,
            255);
    }
    for (uint32_t x = 1; x < 6; ++x) {
        auto box_x_offset = box_width() * x_index + box_padding_x();
        auto bottle_x_offset = box_x_offset + bottle_width() * x;
        auto center_x = bottle_x_offset;
        boxRGBA(
            renderer,
            center_x - 2,
            y_offset + 2,
            center_x + 2,
            y_offset + box_height() - 2,
            32,
            32,
            32,
            255);
    }
    for (uint32_t x = 1; x < 6; ++x) {
        auto box_x_offset = box_width() * x_index + box_padding_x();
        auto bottle_x_offset = box_x_offset + bottle_width() * x;
        auto center_x = bottle_x_offset;
        for (uint32_t y = 1; y < 4; ++y) {
            auto box_y_offset = box_height() * y_index + box_padding_y();
            auto bottle_y_offset = box_y_offset + bottle_height() * y;
            auto center_y = bottle_y_offset;
            auto radius = std::min(bottle_height(), bottle_width()) / 2.5;
            if (x == 3 || y == 2) {
                for (auto r = 0; r < 5; ++r) {
                    circleRGBA(renderer, center_x, center_y, radius - r, 32, 32, 32, 255);
                }
            } else {
                filledCircleRGBA(renderer, center_x, center_y, radius, 32, 32, 32, 255);
            }
        }
    }
}

void Demo::render() {
    SDL_SetRenderDrawColor(renderer, 16, 16, 16, 0);
    SDL_RenderClear(renderer);

    for (uint32_t x = 0; x < setup.boxes_x; ++x) {
        for (uint32_t y = 0; y < setup.boxes_y; ++y) {
            draw_box(x, y);
        }
    }

    SDL_RenderPresent(renderer);
}

void Demo::refresh_scene() {
    scene = scene_by_index(scene_index, &grid);
}

void Demo::process_events() {
    SDL_Event e;
    while (SDL_PollEvent(&e)) {
        if (e.type == SDL_QUIT || (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE)) {
            running = false;
        }
        if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_LEFT) {
            scene_index--;
            refresh_scene();
        }
        if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RIGHT) {
            scene_index++;
            refresh_scene();
        }
    }
}

void Demo::mainloop() {
    int current_time = SDL_GetTicks();
    uint32_t dt = current_time - last_frame_time;
    last_frame_time = current_time;

    process_events();
    scene->update(dt);
    render();
}