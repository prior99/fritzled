#ifndef __DEMO_H
#define __DEMO_H

#include <SDL2/SDL.h>
#include <fritz-config.h>
#include <grid.h>
#include <scenes.h>
#include <setup.h>
#include <memory>

class Demo {
  public:
    Demo(Setup setup, uint32_t width, uint32_t height);
    ~Demo();

    int init();
    void mainloop();
    uint32_t bottle_width();
    uint32_t bottle_height();
    uint32_t box_width();
    uint32_t box_height();
    uint32_t box_padding_x();
    uint32_t box_padding_y();

    uint32_t width();
    uint32_t height();
    uint32_t physical_width();
    uint32_t physical_height();

    void draw_box(uint32_t x_index, uint32_t y_index);
    void draw_bottle(uint32_t box_x_index, uint32_t box_y_index, uint32_t bottle_x_index, uint32_t bottle_y_index);

    bool running = true;

  private:
    void process_events();
    void render();
    void refresh_scene();

    uint32_t scene_index = 0;
    Setup setup;
    Grid grid;
    std::unique_ptr<Scene> scene;
    uint32_t window_width;
    uint32_t window_height;
    SDL_Renderer* renderer;
    SDL_Window* window;
    uint32_t last_frame_time;
};

#endif
