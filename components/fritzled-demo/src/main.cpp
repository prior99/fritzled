#include "demo.h"
#include "grid.h"
#include <fritz-config.h>
#include <grid.h>
#include <setup.h>

#define WIDTH 1024
#define HEIGHT 768

int main() {
    Demo demo(Setup(BOXES_X, BOXES_Y, BOX_BOTTLES_X, BOX_BOTTLES_Y), WIDTH, HEIGHT);
    demo.init();
    while (demo.running) {
        demo.mainloop();
    }
    return 0;
}
