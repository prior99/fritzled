#ifndef __SCENES_H
#define __SCENES_H

#include "scenes/scene.h"
#include "grid.h"
#include <memory>
#include <string>
#include <vector>

std::unique_ptr<Scene> scene_by_index(uint32_t index, Grid* grid);
std::unique_ptr<Scene> scene_by_name(const std::string &name, Grid* grid);
bool has_scene(const std::string &name);
std::string scene_name_by_index(uint32_t index);
uint32_t scene_index_by_name(const std::string &index);
void get_scene_names(std::vector<std::string> &scene_names);

#endif