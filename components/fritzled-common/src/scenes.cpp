#include "scenes.h"
#include "scenes/scene-fading-dots.h"
#include "scenes/scene-flying-dots.h"
#include "scenes/scene-snake.h"
#include <vector>
#include <map>
#include <functional>

static const std::map<std::string, std::function<Scene *(Grid *grid)>> scenes({
    { "fading-dots", [](Grid *grid) { return new SceneFadingDots(grid); } },
    { "flying-dots", [](Grid *grid) { return new SceneFlyingDots(grid); } },
    { "snake", [](Grid *grid) { return new SceneSnake(grid); } },
});

std::unique_ptr<Scene> scene_by_index(uint32_t index, Grid *grid) {
    auto wrapped = index % scenes.size();
    auto counter = 0;
    for (auto it = scenes.begin(); it != scenes.end(); ++it) {
        if (counter == index) {
            return std::unique_ptr<Scene>(it->second(grid));
        }
        counter++;
    }
    return nullptr;
}

std::string scene_name_by_index(uint32_t index) {
    auto wrapped = index % scenes.size();
    auto counter = 0;
    for (auto it = scenes.begin(); it != scenes.end(); ++it) {
        if (counter == index) {
            return it->first;
        }
        counter++;
    }
    return "unknown scene";
}

bool has_scene(const std::string &name) {
    return scenes.find(name) != scenes.end();
}

std::unique_ptr<Scene> scene_by_name(const std::string &name, Grid *grid) {
    return std::unique_ptr<Scene>(scenes.at(name)(grid));
}

uint32_t scene_index_by_name(const std::string &name) {
    auto counter = 0;
    for (auto it = scenes.begin(); it != scenes.end(); ++it) {
        if (it->first == name) {
            return counter;
        }
        counter++;
    }
    return 0;
}

void get_scene_names(std::vector<std::string> &scene_names) {
    for (auto it = scenes.begin(); it != scenes.end(); ++it) {
        scene_names.push_back(it->first);
    }
}