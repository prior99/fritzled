#include "setup.h"
#include <cstdint>

Setup::Setup(uint32_t boxes_x, uint32_t boxes_y, uint32_t box_width, uint32_t box_height)
    : boxes_x(boxes_x), boxes_y(boxes_y), box_width(box_width), box_height(box_height) {
}

Setup::Setup() : boxes_x(0), boxes_y(0), box_width(0), box_height(0) {
}

uint32_t Setup::width() {
    return boxes_x * box_width;
}

uint32_t Setup::height() {
    return boxes_y * box_height;
}

uint32_t Setup::bottles() {
    return width() * height();
}

uint32_t Setup::bottles_per_box() {
    return box_height * box_width;
}

uint32_t Setup::get_box_offset(uint32_t box_x, uint32_t box_y) {
    if (box_y % 2 == 0) {
        return bottles_per_box() * (boxes_x * box_y + box_x);
    } else {
        return bottles_per_box() * (boxes_x * box_y + (boxes_x - 1 - box_x));
    }
}

uint32_t Setup::get_bottle_offset_in_box(uint32_t bottle_x, uint32_t bottle_y) {
    if (bottle_y % 2 == 0) {
        return box_width * bottle_y + bottle_x;
    } else {
        return box_width * bottle_y + (box_width - 1 - bottle_x);
    }
}

uint32_t Setup::get_bottle_index(uint32_t box_x, uint32_t box_y, uint32_t bottle_x, uint32_t bottle_y) {
    return get_box_offset(box_x, box_y) + get_bottle_offset_in_box(bottle_x, bottle_y);
}

uint32_t Setup::boxes() {
    return boxes_x * boxes_y;
}

void Setup::change(uint32_t boxes_x, uint32_t boxes_y, uint32_t box_width, uint32_t box_height) {
    this->boxes_x = boxes_x;
    this->boxes_y = boxes_y;
    this->box_width = box_width;
    this->box_height = box_height;
}