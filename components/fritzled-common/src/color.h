#ifndef __COLOR_H
#define __COLOR_H

#include <array>
#include <cstdint>
#include <string>

union hsv_t {
    struct {
        float h;
        float s;
        float v;
    };
    std::array<float, 3> array;
};

union rgb_t {
    struct {
        uint8_t r;
        uint8_t g;
        uint8_t b;
    };
    std::array<uint8_t, 3> array;
};

union grbw_t {
    struct {
        uint8_t g;
        uint8_t r;
        uint8_t b;
        uint8_t w;
    };
    std::array<uint8_t, 4> array;
};

union rgbw_t {
    struct {
        uint8_t r;
        uint8_t g;
        uint8_t b;
        uint8_t w;
    };
    std::array<uint8_t, 4> array;
};

class Color {
  public:
    Color();
    Color(const std::string &str);
    ~Color();
    void set_rgbw(uint8_t r, uint8_t g, uint8_t b, uint8_t w);
    void set_rgb(uint8_t r, uint8_t g, uint8_t b);
    rgbw_t get_rgbw();
    rgb_t get_rgb();
    grbw_t get_grbw();
    void set_hsv(float h, float s, float l);
    hsv_t get_hsv();
    uint8_t saturation();
    uint8_t get_white();
    bool is_black();
    void set_color(Color &color);
    void set_black();
    uint32_t get_int();
    std::string get_string();
    void set_int(uint32_t value);
    void set_string(const std::string &str);

  private:
    grbw_t colors;
};

#endif
