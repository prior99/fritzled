#include "color.h"
#include <array>
#include <cmath>
#include <cstring>
#include <iomanip>
#include <sstream>
#include <string>

Color::Color() : colors({{.g = 0, .r = 0, .b = 0, .w = 0}}) {
}

Color::Color(const std::string &str) {
    set_string(str);
}

Color::~Color() {
}

void Color::set_rgbw(uint8_t r, uint8_t g, uint8_t b, uint8_t w) {
    colors.r = r;
    colors.g = g;
    colors.b = b;
    colors.w = w;
}

grbw_t Color::get_grbw() {
    return colors;
}

rgbw_t Color::get_rgbw() {
    return {colors.r, colors.g, colors.b, colors.w};
}

rgb_t Color::get_rgb() {
    return {colors.r, colors.g, colors.b};
}

uint8_t saturation(rgb_t color) {
    float max = std::max(color.r, std::max(color.g, color.b));
    float min = std::min(color.r, std::min(color.g, color.b));
    return round(100 * ((max - min) / max));
}

// Returns the value of white.
uint8_t get_white(rgb_t color) {
    return (255 - saturation(color)) / 255 * (color.r + color.g + color.b) / 3;
}

// Example function.
rgbw_t rgb_to_rgbw(rgb_t input) {
    return {{.r = input.r, .g = input.g, .b = input.b, .w = get_white(input)}};
}

void Color::set_rgb(uint8_t r, uint8_t g, uint8_t b) {
    rgbw_t rgbw = rgb_to_rgbw({{.r = r, .g = g, .b = b}});
    colors.r = rgbw.r;
    colors.g = rgbw.g;
    colors.b = rgbw.b;
    colors.w = rgbw.w;
}

void Color::set_hsv(float h, float s, float v) {
    float C = s * v;
    float X = C * (1.f - fabs(fmod(h / 60.0, 2) - 1.f));
    float m = v - C;
    float r, g, b;

    if (h >= 0 && h < 60) {
        r = C;
        g = X;
        b = 0;
    } else if (h >= 60 && h < 120) {
        r = X;
        g = C;
        b = 0;
    } else if (h >= 120 && h < 180) {
        r = 0;
        g = C;
        b = X;
    } else if (h >= 180 && h < 240) {
        r = 0;
        g = X;
        b = C;
    } else if (h >= 240 && h < 300) {
        r = X;
        g = 0;
        b = C;
    } else {
        r = C;
        g = 0;
        b = X;
    }

    set_rgb(
        static_cast<uint8_t>((r + m) * 255), static_cast<uint8_t>((g + m) * 255), static_cast<uint8_t>((b + m) * 255));
}

hsv_t Color::get_hsv() {
    float r = static_cast<float>(colors.r) / 255;
    float g = static_cast<float>(colors.g) / 255;
    float b = static_cast<float>(colors.b) / 255;
    float max = std::max(std::max(r, g), b);
    float min = std::min(std::min(r, g), b);
    float range = max - min;
    float h, s, v;

    if (range > 0) {
        if (max == r) {
            h = 60 * (fmod(((g - b) / range), 6));
        } else if (max == g) {
            h = 60 * (((b - r) / range) + 2);
        } else if (max == b) {
            h = 60 * (((r - g) / range) + 4);
        }

        if (max > 0) {
            s = range / max;
        } else {
            s = 0;
        }

        v = max;
    } else {
        h = 0;
        s = 0;
        v = max;
    }

    if (h < 0) {
        h = 360 + h;
    }

    return {{.h = h, .s = s, .v = v}};
}

bool Color::is_black() {
    return colors.r == 0 && colors.b == 0 && colors.b == 0 && colors.w == 0;
}

void Color::set_black() {
    colors.r = 0;
    colors.g = 0;
    colors.b = 0;
    colors.w = 0;
}

void Color::set_color(Color &color) {
    memcpy(colors.array.data(), reinterpret_cast<void *>(color.get_grbw().array.data()), 4);
}

uint32_t Color::get_int() {
    rgb_t rgb = get_rgb();
    return (rgb.r << 16) | (rgb.g << 8) | rgb.b;
}

std::string Color::get_string() {
    std::stringstream stream;
    stream << "#" << std::setfill('0') << std::setw(6) << std::hex << get_int();
    return stream.str();
}

void Color::set_string(const std::string &str) {
    uint32_t value;
    std::stringstream stream;
    stream << std::hex << str.substr(1);
    stream >> value;
    set_int(value);
}

void Color::set_int(uint32_t value) {
    set_rgb(
        static_cast<uint8_t>((value >> 16) & 0xFF),
        static_cast<uint8_t>((value >> 8) & 0xFF),
        static_cast<uint8_t>(value & 0xFF));
}