#ifndef __SETUP_H
#define __SETUP_H

#include <cstdint>

class Setup {
  public:
    Setup();
    Setup(uint32_t boxes_x, uint32_t boxes_y, uint32_t box_width = 4, uint32_t box_height = 6);
    Setup(const Setup &other) = default;
    Setup(Setup &&other) noexcept = default;
    ~Setup() = default;

    uint32_t width();
    uint32_t height();
    uint32_t bottles();
    uint32_t bottles_per_box();
    uint32_t boxes();
    uint32_t get_box_offset(uint32_t box_x, uint32_t box_y);
    uint32_t get_bottle_offset_in_box(uint32_t bottle_x, uint32_t bottle_y);
    uint32_t get_bottle_index(uint32_t box_x, uint32_t box_y, uint32_t bottle_x, uint32_t bottle_y);

    void change(uint32_t boxes_x, uint32_t boxes_y, uint32_t box_width, uint32_t box_height);

    uint32_t boxes_x;
    uint32_t boxes_y;
    uint32_t box_width;
    uint32_t box_height;

    static const uint8_t version = 1;
};

#endif