#ifndef __SCENE_SNAKE_H
#define __SCENE_SNAKE_H

#include "../grid.h"
#include "scene.h"
#include <cmath>
#include <random>

class SceneSnake : public Scene {
  public:
    SceneSnake(Grid *grid) : Scene(grid) {
    }

    ~SceneSnake() {
    }

  protected:
    void render(uint32_t delta_t_ms) {
        last_dot_time += delta_t_ms;
        if (last_dot_time > 500) {
            last_dot_time = 0;
            current_x++;
            if (current_x == setup->width()) {
                current_y++;
                current_x = 0;
                if (current_y == setup->height()) {
                    current_y = 0;
                }
            }
        }
        for (int x = 0; x >= setup->width(); ++x) {
            for (auto y = 0; y < setup->height(); ++y) {
                if (x == current_x && y == current_y) {
                    grid->get_color(x, y)->set_hsv(100, 1, 1);
                } else {
                    grid->get_color(x, y)->set_hsv(0, 0, 0);
                }
            }
        }
    }

  private:
    uint32_t current_x = 0;
    uint32_t current_y = 0;
    uint32_t last_dot_time = 0;
};

#endif