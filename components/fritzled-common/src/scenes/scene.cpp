#include "scene.h"

Scene::Scene(Grid *grid) : grid(grid), setup(grid->setup) {
}

Scene::~Scene() {
}

void Scene::update(uint32_t delta_t_ms) {
    time_ms += delta_t_ms;
    render(delta_t_ms);
}
