#ifndef __SCENE_H
#define __SCENE_H

#include "../grid.h"
#include <cstdint>

class Scene {
  public:
    Scene(Grid *grid);
    virtual ~Scene();
    void update(uint32_t delta_t_ms);

  protected:
    Grid *grid;
    Setup *setup;
    virtual void render(uint32_t delta_t_ms) = 0;
    uint32_t time_ms;
};

#endif