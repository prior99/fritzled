#ifndef __SCENE_FADING_DOTS_H
#define __SCENE_FADING_DOTS_H

#include "../grid.h"
#include "scene.h"
#include <cmath>
#include <random>

class SceneFadingDots : public Scene {
  public:
    SceneFadingDots(Grid *grid)
        : Scene(grid),
          rand_x(0, setup->width() - 1),
          rand_y(0, setup->height() - 1),
          rand_interval(300, 1000),
          rand_amount(1, 3),
          rand_hue(0.f, 360.f),
          rand_saturation(0.f, .5f),
          next_interval(rand_interval(random_engine)) {
    }
    ~SceneFadingDots() {
    }

  protected:
    void render(uint32_t delta_t_ms) {
        last_dot_time += delta_t_ms;
        if (last_dot_time > next_interval) {
            auto amount = rand_amount(random_engine);
            uint32_t x;
            uint32_t y;
            for (auto i = 0; i < amount; i++) {
                do {
                    x = rand_x(random_engine);
                    y = rand_y(random_engine);
                } while (grid->get_color(x, y)->get_hsv().v != 0);
                last_dot_time = 0;
                grid->get_color(x, y)->set_hsv(
                    rand_hue(random_engine), rand_saturation(random_engine) + 0.5f, 1);
            }
            next_interval = rand_interval(random_engine);
        }
        for (auto x = 0; x < setup->width(); ++x) {
            for (auto y = 0; y < setup->height(); ++y) {
                auto color = grid->get_color(x, y)->get_hsv();
                grid->get_color(x, y)->set_hsv(
                    color.h, color.s, std::max(color.v - 0.0001f * static_cast<float>(delta_t_ms), 0.0f));
            }
        }
    };

  private:
    std::default_random_engine random_engine;
    std::uniform_int_distribution<uint32_t> rand_x;
    std::uniform_int_distribution<uint32_t> rand_y;
    std::uniform_int_distribution<uint32_t> rand_interval;
    std::uniform_int_distribution<uint32_t> rand_amount;
    std::uniform_real_distribution<float> rand_hue;
    std::uniform_real_distribution<float> rand_saturation;
    uint32_t next_interval;
    uint32_t last_dot_time = 0;
};

#endif