#ifndef __SCENE_FLYING_DOTS_H
#define __SCENE_FLYING_DOTS_H

#include "../grid.h"
#include "scene.h"
#include <random>
#include <cmath>

class SceneFlyingDots : public Scene {
  public:
    SceneFlyingDots(Grid *grid)
        : Scene(grid),
          rand_y(0, setup->height() - 1),
          rand_hue(0.f, 360.f),
          rand_saturation(0.f, .5f),
          rand_interval(100, 1500),
          next_interval(rand_interval(random_engine)) {
    }
    ~SceneFlyingDots() {
    }

  protected:
    void render(uint32_t delta_t_ms) {
        last_dot_time += delta_t_ms;
        last_fly_time += delta_t_ms;
        if (last_dot_time > next_interval) {
            uint32_t y = rand_y(random_engine);
            last_dot_time = 0;
            grid->get_color(0, y)->set_hsv(
                rand_hue(random_engine), rand_saturation(random_engine) + 0.5f, 1);
            next_interval = rand_interval(random_engine);
        }
        if (last_fly_time > 50) {
            last_fly_time = 0;
            for (int x = setup->width() - 1; x >= 0; --x) {
                for (auto y = 0; y < setup->height(); ++y) {
                    Color *color = grid->get_color(x, y);
                    if (color->is_black()) {
                        continue;
                    }
                    if (x < setup->width() - 1) {
                        Color *right = grid->get_color(x + 1, y);
                        right->set_color(*color);
                    }
                    auto hsv = color->get_hsv();
                    color->set_hsv(hsv.h, std::max(0.1f, hsv.s - 0.01f), std::max(0.f, hsv.v - 0.2f));
                }
            }
        }
    };

  private:
    std::default_random_engine random_engine;
    std::uniform_int_distribution<uint32_t> rand_y;
    std::uniform_real_distribution<float> rand_hue;
    std::uniform_real_distribution<float> rand_saturation;
    std::uniform_real_distribution<float> rand_value;
    std::uniform_int_distribution<uint32_t> rand_interval;
    uint32_t next_interval;
    uint32_t last_dot_time = 0;
    uint32_t last_fly_time = 0;
};

#endif