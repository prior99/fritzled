#include "box.h"

Box::Box(Setup *setup) : setup(setup), colors(std::vector<Color>(setup->bottles_per_box())) {
}

Box::Box(const Box &box) : setup(box.setup), colors(box.colors) {
}
Box::Box(Box &&box) noexcept : setup(std::move(box.setup)), colors(std::move(box.colors)) {
}

Color *Box::get_color(uint32_t x, uint32_t y) {
    return &colors[y * setup->box_width + x];
}
