#ifndef __GRID_H
#define __GRID_H

#include "setup.h"
#include "box.h"
#include "color.h"
#include <json11.hpp>

class Grid {
    public:
        Grid(Setup *setup);
        Grid(const Grid &other) = default;
        Grid(Grid &&other) noexcept;
        ~Grid() = default;
        Box *get_box(uint32_t x, uint32_t y);
        Color *get_color(uint32_t x, uint32_t y);
        void fill_rgbw(uint8_t r, uint8_t g, uint8_t b, uint8_t w);
        json11::Json to_json();
        void reset();

        Setup *setup;

    private:
        std::vector<Box> boxes;
};

#endif