#ifndef __BOX_H
#define __BOX_H

#include <cstdint>
#include "setup.h"
#include "color.h"
#include <memory>
#include <vector>

class Box {
    public:
        Box(Setup *setup);
        Box(const Box &box);
        Box(Box &&box) noexcept;
        ~Box() = default;
        Color *get_color(uint32_t x, uint32_t y);

    private:
        Setup *setup;
        std::vector<Color> colors;
};

#endif
