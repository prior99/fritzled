#include "grid.h"

Grid::Grid(Setup *setup) : setup(setup), boxes(setup->boxes(), setup) {
}

Grid::Grid(Grid &&other) noexcept : setup(std::move(other.setup)), boxes(std::move(other.boxes)) {
}

json11::Json Grid::to_json() {
    using json11::Json;

    Json::array result_array = Json::array();
    for (auto y = 0; y < setup->height(); ++y) {
        for (auto x = 0; x < setup->width(); ++x) {
            result_array.push_back(get_color(x, y)->get_string());
        }
    }
    return Json::object{
        {"rgb", result_array},
        {"width", static_cast<double>(setup->width())},
        {"height", static_cast<double>(setup->height())},
    };
}

void Grid::fill_rgbw(uint8_t r, uint8_t g, uint8_t b, uint8_t w) {
    for (auto x = 0; x < setup->width(); ++x) {
        for (auto y = 0; y < setup->height(); ++y) {
            get_color(x, y)->set_rgbw(r, g, b, w);
        }
    }
}

Box *Grid::get_box(uint32_t x, uint32_t y) {
    return &boxes[x + y * setup->boxes_x];
}

Color *Grid::get_color(uint32_t x, uint32_t y) {
    return get_box(x / setup->box_width, y / setup->box_height)->get_color(x % setup->box_width, y % setup->box_height);
}

void Grid::reset() {
    boxes.clear();
    for (auto i = 0; i < setup->boxes(); ++i) {
        boxes.push_back(Box(setup));
    }
}