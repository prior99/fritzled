cmake_minimum_required(VERSION 3.5)

project(fritzled-common CXX)

set(CMAKE_CXX_FLAGS, "-g -Wall -Wextra -pedantic -Werror -std=c++17 -Wno-unused-parameter")

file(GLOB_RECURSE CPP_FILES src/*.cpp src/*.h)

add_library(${PROJECT_NAME} STATIC ${CPP_FILES})

include_directories(SYSTEM ${json11_INCLUDE_DIRS})

target_link_libraries(${PROJECT_NAME})

set(${PROJECT_NAME}_INCLUDE_DIRS ${PROJECT_SOURCE_DIR}/src CACHE INTERNAL "${PROJECT_NAME}: Include Directories" FORCE)