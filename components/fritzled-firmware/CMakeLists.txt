cmake_minimum_required(VERSION 3.5)

project(fritzled-firmware C CXX ASM)

set(CMAKE_CXX_FLAGS, "-g -Wall -Wextra -pedantic -Werror -std=c++17 -Wno-unused-parameter")

include($ENV{IDF_PATH}/tools/cmake/idf.cmake)

idf_build_process(esp32
    COMPONENTS esp32 esp_http_server nvs_flash freertos esptool_py mqtt
    SDKCONFIG ${CMAKE_BINARY_DIR}/sdkconfig
    SDKCONFIG_DEFAULTS ${PROJECT_SOURCE_DIR}/sdkconfig.defaults
    PROJECT_DIR ${PROJECT_SOURCE_DIR}
    BUILD_DIR ${CMAKE_BINARY_DIR}/components/fritzled-firmware
)

file(GLOB_RECURSE sources src/*.cpp src/*.h)

set(elf_file ${CMAKE_PROJECT_NAME}.elf)
add_executable(${elf_file} ${sources})

include_directories(SYSTEM ${SDL2_INCLUDE_DIRS} ${fritzled-common_INCLUDE_DIRS} ${json11_INCLUDE_DIRS})

target_link_libraries(${elf_file} idf::esp32 idf::freertos idf::esp_http_server idf::spi_flash idf::nvs_flash idf::mqtt fritzled-common json11)
idf_build_executable(${elf_file})


set(CMAKE_EXPORT_COMPILE_COMMANDS 1)