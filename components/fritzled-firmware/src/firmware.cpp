#include "firmware.h"
#include "scenes.h"
#include <esp_err.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <grid.h>
#include <json11.hpp>

static const char *TAG = "firmware";

Firmware::Firmware(Setup &&setup, SK6812 sk6812)
    : setup(setup), grid(Grid(&this->setup)), sk6812(sk6812), scene(scene_by_index(0, &grid)) {
}

void Firmware::show_color(uint8_t r, uint8_t g, uint8_t b, uint8_t w, uint32_t time_ms) {
    grid.fill_rgbw(r, g, b, w);
    render();
    vTaskDelay(time_ms / portTICK_PERIOD_MS);
}

void Firmware::init() {
    ESP_LOGI(
        TAG,
        "Boxes: %dx%d (=%d). Bottles per Box: %dx%d (=%d). Total size: %dx%d (=%d).",
        setup.boxes_x,
        setup.boxes_y,
        setup.boxes(),
        setup.box_width,
        setup.box_height,
        setup.bottles_per_box(),
        setup.width(),
        setup.height(),
        setup.bottles());
    ESP_LOGI(TAG, "Initializing SK6812 driver...");
    sk6812.init();
    if (setup.bottles() == 0) {
        ESP_LOGW(TAG, "Total size is 0. Skipping test images and scene initialization.");
        return;
    }
    test_images();
    running = true;
}

void Firmware::test_images() {
    show_color(0, 0, 0, 255, 200);
    show_color(0, 0, 255, 0, 200);
    show_color(0, 255, 0, 0, 200);
    show_color(255, 0, 0, 0, 200);
    grid.fill_rgbw(0, 0, 0, 0);
    render();
}

void Firmware::main_loop(uint32_t delta_t_ms) {
    if (!running) {
        return;
    }
    scene->update(delta_t_ms);
    render();
}

void Firmware::render() {
    if (setup.bottles() == 0) {
        return;
    }
    for (auto box_x = 0; box_x < setup.boxes_x; box_x++) {
        for (auto box_y = 0; box_y < setup.boxes_y; box_y++) {
            for (auto bottle_x = 0; bottle_x < setup.box_width; bottle_x++) {
                for (auto bottle_y = 0; bottle_y < setup.box_height; bottle_y++) {
                    auto index = setup.get_bottle_index(box_x, box_y, bottle_x, bottle_y);
                    sk6812.set_color(index, grid.get_box(box_x, box_y)->get_color(bottle_x, bottle_y));
                }
            }
        }
    }
    sk6812.refresh();
}

void Firmware::on_next_scene() {
    if (!running) {
        ESP_LOGI(TAG, "Ignoring request to change scene as animation is paused.");
        return;
    }
    scene_index++;
    ESP_LOGI(TAG, "Changing scene to \"%s\".", get_scene_name());
    refresh_scene();
    this->publish_to_mqtt();
}

void Firmware::on_previous_scene() {
    if (!running) {
        ESP_LOGI(TAG, "Ignoring request to change scene as animation is paused.");
        return;
    }
    scene_index--;
    ESP_LOGI(TAG, "Changing scene to \"%s\".", get_scene_name());
    refresh_scene();
    this->publish_to_mqtt();
}

void Firmware::on_standby() {
    if (running) {
        ESP_LOGI(TAG, "Entering standby mode (animation paused and display set to black).");
        grid.fill_rgbw(0, 0, 0, 0);
        render();
        running = false;
    } else {
        ESP_LOGI(TAG, "Resuming from standby mode.");
        refresh_scene();
    }
    this->publish_to_mqtt();
}

void Firmware::refresh_scene() {
    running = false;
    grid.fill_rgbw(0, 0, 0, 0);
    scene = scene_by_index(scene_index, &grid);
    running = true;
}

uint32_t Firmware::get_scene_index() {
    return scene_index;
}

const std::string Firmware::get_scene_name() {
    return scene_name_by_index(this->scene_index);
}

void Firmware::set_scene(const std::string &name) {
    if (!running) {
        return;
    }
    scene_index = scene_index_by_name(name);
    refresh_scene();
    publish_to_mqtt();
}

Setup *Firmware::get_setup() {
    return &setup;
}

Grid *Firmware::get_grid() {
    return &grid;
}

void Firmware::change_setup(uint32_t boxes_x, uint32_t boxes_y, uint32_t bottles_x, uint32_t bottles_y) {
    ESP_LOGI(
        TAG,
        "Changing current setup from %dx%d boxes and %dx%d bottles to %dx%d boxes and %dx%d bottles.",
        setup.boxes_x,
        setup.boxes_y,
        setup.box_width,
        setup.box_height,
        boxes_x,
        boxes_y,
        bottles_x,
        bottles_y);
    running = false;
    setup.change(boxes_x, boxes_y, bottles_x, bottles_y);
    grid.reset();
    sk6812.reset(setup.bottles());
    test_images();
    refresh_scene();
}

json11::Json Firmware::to_json() {
    using json11::Json;
    Json json_setup = Json::object{
        {"boxesX", static_cast<double>(setup.boxes_x)},
        {"boxesY", static_cast<double>(setup.boxes_y)},
        {"bottlesX", static_cast<double>(setup.box_width)},
        {"bottlesY", static_cast<double>(setup.box_height)},
    };
    return Json::object{{"scene", get_scene_name()}, {"setup", json_setup}, {"running", running}};
}

void Firmware::publish_to_mqtt() {
    using json11::Json;
    if (this->mqtt_client == nullptr) {
        ESP_LOGE(TAG, "Attempted to publish to MQTT, but MQTT is not setup.");
        return;
    }
    auto running = this->running ? "ON" : "OFF";
    Json state = Json::object{
        {"effect", this->get_scene_name()},
        {"state", running},
    };
    ESP_LOGI(TAG, "Publishing state to MQTT...");
    esp_mqtt_client_publish(this->mqtt_client, "home/fritzled", state.dump().c_str(), 0, 0, 0);
}

void Firmware::set_mqtt(esp_mqtt_client_handle_t mqtt_client) {
    this->mqtt_client = mqtt_client;
    this->publish_to_mqtt();
}