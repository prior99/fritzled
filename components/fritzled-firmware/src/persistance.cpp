#include "persistance.h"
#include <esp_log.h>
#include <nvs.h>
#include <nvs_flash.h>
#include <cstring>

#define STORAGE_NAMESPACE "fritzled"

static const char *TAG = "persistance";

persistance_error_t save_generic(void *data, size_t size, std::string prefix, uint8_t app_version) {
    nvs_handle_t nvs_handle;
    esp_err_t err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &nvs_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Unable to open NVS for writing.");
        nvs_close(nvs_handle);
        return PERSISTANCE_ERROR;
    }
    std::string version_name = prefix + "_ver";
    err = nvs_set_u8(nvs_handle, version_name.c_str(), app_version);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Failed to save %s version to NVS.", prefix.c_str());
        nvs_close(nvs_handle);
        return PERSISTANCE_ERROR;
    }
    err = nvs_set_blob(nvs_handle, prefix.c_str(), data, size);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Failed to save %s data to NVS.", prefix.c_str());
        nvs_close(nvs_handle);
        return PERSISTANCE_ERROR;
    }
    ESP_LOGI(TAG, "%s saved to NVS.", prefix.c_str());
    nvs_commit(nvs_handle);
    nvs_close(nvs_handle);
    return PERSISTANCE_OK;
}

persistance_error_t load_generic(void *data, size_t size, std::string prefix, uint8_t app_version) {
    nvs_handle_t nvs_handle;
    esp_err_t err = nvs_open(STORAGE_NAMESPACE, NVS_READONLY, &nvs_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Unable to open NVS for reading: %s", esp_err_to_name(err));
        nvs_close(nvs_handle);
        return PERSISTANCE_ERROR;
    }
    uint8_t nvs_version = 0;
    std::string version_name = prefix + "_ver";
    err = nvs_get_u8(nvs_handle, version_name.c_str(), &nvs_version);
    if (err != ESP_OK) {
        nvs_close(nvs_handle);
        if (err == ESP_ERR_NVS_NOT_FOUND) {
            ESP_LOGI(TAG, "%s version not found in NVS.", prefix.c_str());
            return PERSISTANCE_UNINITIALIZED;
        }
        ESP_LOGE(TAG, "%s version could not be loaded from NVS.", prefix.c_str());
        return PERSISTANCE_ERROR;
    }
    if (nvs_version != app_version) {
        nvs_close(nvs_handle);
        ESP_LOGI(TAG, "App version of %s is %d, but version in NVS is %d.", prefix.c_str(), app_version, nvs_version);
        return PERSISTANCE_INCOMPATIBLE;
    }
    size_t nvs_size = size;
    err = nvs_get_blob(nvs_handle, prefix.c_str(), data, &nvs_size);
    if (err != ESP_OK) {
        nvs_close(nvs_handle);
        if (err == ESP_ERR_NVS_NOT_FOUND) {
            ESP_LOGI(TAG, "%s data not found in NVS.", prefix.c_str());
            return PERSISTANCE_UNINITIALIZED;
        }
        ESP_LOGE(TAG, "%s data could not be loaded from NVS: %s", prefix.c_str(), esp_err_to_name(err));
        return PERSISTANCE_ERROR;
    }
    if (nvs_size != size) {
        ESP_LOGE(TAG, "%s data in NVS has unexpected size %d. Expected size was %d.", prefix.c_str(), nvs_size, size);
        nvs_close(nvs_handle);
        return PERSISTANCE_ERROR;
    }
    nvs_close(nvs_handle);
    return PERSISTANCE_OK;
}

Persistance::Persistance() {
}

Persistance::~Persistance() {
}

persistance_error_t Persistance::init() {
    ESP_LOGI(TAG, "Initializing NVS...");
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NOT_FOUND) {
        ESP_LOGE(TAG, "NVS partition missing.");
        return PERSISTANCE_ERROR;
    }
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_LOGW(TAG, "NVS truncated or incompatible.");
        ESP_LOGI(TAG, "Erasing and re-initializing NVS.");
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
        if (err != ESP_OK) {
            ESP_LOGE(TAG, "Failed to erase and re-initialize NVS.");
            return PERSISTANCE_ERROR;
        }
    }
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Unknown error when trying to initialize NVS: %s", esp_err_to_name(err));
        return PERSISTANCE_ERROR;
    }
    nvs_handle_t nvs_handle;
    err = nvs_open(STORAGE_NAMESPACE, NVS_READONLY, &nvs_handle);
    if (err == ESP_ERR_NVS_NOT_FOUND) {
        ESP_LOGI(TAG, "NVS namespace not found. Creating a new one...");
        err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &nvs_handle);
        if (err != ESP_OK) {
            ESP_LOGE(TAG, "Unable to create namespace in NVS: %s", esp_err_to_name(err));
            return PERSISTANCE_ERROR;
        }
        nvs_commit(nvs_handle);
        nvs_close(nvs_handle);
    }
    ESP_LOGI(TAG, "NVS ready.");
    return PERSISTANCE_OK;
}



persistance_error_t Persistance::load_setup(Setup &setup) {
    return load_generic(&setup, sizeof(Setup), "setup", Setup::version);
}

persistance_error_t Persistance::save_setup(Setup &setup) {
    return save_generic(&setup, sizeof(setup), "setup", Setup::version);
}

persistance_error_t Persistance::wipe() {
    nvs_handle_t nvs_handle;
    esp_err_t err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &nvs_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Unable to open NVS for writing:", esp_err_to_name(err));
        nvs_close(nvs_handle);
        return PERSISTANCE_ERROR;
    }
    err = nvs_erase_all(nvs_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Unable to erase NVS: %s.", esp_err_to_name(err));
        nvs_close(nvs_handle);
        return PERSISTANCE_ERROR;
    }
    err = nvs_commit(nvs_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Unable to commit wipe of NVS: %s.", esp_err_to_name(err));
        nvs_close(nvs_handle);
        return PERSISTANCE_ERROR;
    }
    nvs_close(nvs_handle);
    return PERSISTANCE_OK;
}