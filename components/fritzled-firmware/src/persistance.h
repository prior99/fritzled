#ifndef __PERSISTANCE_H
#define __PERSISTANCE_H

#include <setup.h>
#include <string>

enum persistance_error_t {
    PERSISTANCE_OK,
    PERSISTANCE_ERROR,
    PERSISTANCE_UNINITIALIZED,
    PERSISTANCE_INCOMPATIBLE,
};

class Persistance {
  public:
    Persistance();
    ~Persistance();
    persistance_error_t init();
    persistance_error_t load_setup(Setup &setup);
    persistance_error_t save_setup(Setup &setup);
    persistance_error_t wipe();
};

#endif