#ifndef __HARDWARE_H
#define __HARDWARE_H

#include <driver/rmt.h>
#include <driver/gpio.h>
#include <esp_attr.h>
#include <esp_log.h>
#include <esp_system.h>
#include <freertos/queue.h>
#include <freertos/task.h>
#include "firmware.h"

#define RMT_CHANNEL RMT_CHANNEL_0
#define LED_COUNT 48
#define PIN_DATA_OUT GPIO_NUM_18
#define PIN_STATUS_LED GPIO_NUM_22
#define PIN_BUTTON_1 GPIO_NUM_25
#define PIN_BUTTON_2 GPIO_NUM_26
#define PIN_BUTTON_3 GPIO_NUM_27

static xQueueHandle gpio_evt_queue = NULL;

enum button_code_t {
    BUTTON_1,
    BUTTON_2,
    BUTTON_3,
    BUTTON_UNKNOWN,
};

button_code_t button_by_pin(gpio_num_t pin);

void IRAM_ATTR gpio_isr_handler(void *arg);

void button_task(void *arg);

void setup_gpio(Firmware *firmware);

void indicate_running();

#endif