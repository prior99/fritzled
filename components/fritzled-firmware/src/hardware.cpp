#include "hardware.h"
#include <esp_log.h>
#include <esp_err.h>

static const char* TAG = "hardware";

button_code_t button_by_pin(gpio_num_t pin) {
    switch (pin) {
        case PIN_BUTTON_1: return BUTTON_1;
        case PIN_BUTTON_2: return BUTTON_2;
        case PIN_BUTTON_3: return BUTTON_3;
        default: return BUTTON_UNKNOWN;
    }
}

void IRAM_ATTR gpio_isr_handler(void *arg) {
    button_code_t button_code = button_by_pin((gpio_num_t)(uint32_t)arg);
    xQueueSendFromISR(gpio_evt_queue, &button_code, NULL);
}

void button_task(void *arg) {
    Firmware *firmware = reinterpret_cast<Firmware *>(arg);
    button_code_t button_code;
    auto last_tick = xTaskGetTickCount();
    for (;;) {
        if (xQueueReceive(gpio_evt_queue, &button_code, portMAX_DELAY)) {
            auto current_tick = xTaskGetTickCount();
            auto diff_ms = (current_tick - last_tick) * portTICK_RATE_MS;
            if (diff_ms > 500) {
                last_tick = current_tick;
                ESP_LOGI(TAG, "Hardware button with code %d pressed.", button_code);
                switch (button_code) {
                    case BUTTON_1: firmware->on_standby(); break;
                    case BUTTON_2: firmware->on_previous_scene(); break;
                    case BUTTON_3: firmware->on_next_scene(); break;
                }
            }
        }
    }
}

void setup_gpio(Firmware *firmware) {
    ESP_LOGI(TAG, "Configuring GPIO outputs...");
    gpio_config_t output_config;
    output_config.intr_type = GPIO_INTR_DISABLE;
    output_config.mode = GPIO_MODE_OUTPUT;
    output_config.pin_bit_mask = 1ULL << PIN_STATUS_LED;
    output_config.pull_down_en = GPIO_PULLDOWN_DISABLE;
    output_config.pull_up_en = GPIO_PULLUP_DISABLE;
    gpio_config(&output_config);
    gpio_set_level(PIN_STATUS_LED, 0);

    ESP_LOGI(TAG, "Configuring GPIO inputs...");
    gpio_config_t input_config;
    input_config.intr_type = GPIO_INTR_POSEDGE;
    input_config.pin_bit_mask = (1ULL << PIN_BUTTON_1) | (1ULL << PIN_BUTTON_2) | (1ULL << PIN_BUTTON_3);
    input_config.mode = GPIO_MODE_INPUT;
    input_config.pull_up_en = GPIO_PULLUP_ENABLE;
    input_config.pull_down_en = GPIO_PULLDOWN_DISABLE;
    gpio_config(&input_config);
    gpio_set_intr_type(PIN_BUTTON_1, GPIO_INTR_NEGEDGE);
    gpio_set_intr_type(PIN_BUTTON_2, GPIO_INTR_NEGEDGE);
    gpio_set_intr_type(PIN_BUTTON_3, GPIO_INTR_NEGEDGE);

    ESP_LOGI(TAG, "Installing interrupt handlers for hardware buttons...");
    gpio_evt_queue = xQueueCreate(10, sizeof(button_code_t));
    xTaskCreate(button_task, "button_task", 2048, reinterpret_cast<void *>(firmware), 10, NULL);
    gpio_install_isr_service(0);
    gpio_isr_handler_add(PIN_BUTTON_1, gpio_isr_handler, (void *)PIN_BUTTON_1);
    gpio_isr_handler_add(PIN_BUTTON_2, gpio_isr_handler, (void *)PIN_BUTTON_2);
    gpio_isr_handler_add(PIN_BUTTON_3, gpio_isr_handler, (void *)PIN_BUTTON_3);

    ESP_LOGI(TAG, "Hardware initialized.");
}

void indicate_running() {
    gpio_set_level(PIN_STATUS_LED, 1);
}