#include "driver/gpio.h"
#include "driver/rmt.h"
#include "esp_attr.h"
#include "esp_spi_flash.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <cstring>

#include "sk6812.h"

static uint32_t ticks_t0h;
static uint32_t ticks_t0l;
static uint32_t ticks_t1h;
static uint32_t ticks_t1l;

static const char *TAG = "sk6812";

const rmt_item32_t create_rmt_item32(bool val) {
    if (val) {
        return {{{ticks_t1h, 1, ticks_t1l, 0}}};
    }
    return {{{ticks_t0h, 1, ticks_t0l, 0}}};
}

static void IRAM_ATTR rmt_translator(
    const void *src,
    rmt_item32_t *dest,
    size_t src_size,
    size_t wanted_num,
    size_t *translated_size,
    size_t *item_num) {
    size_t size = 0;
    size_t num = 0;
    uint8_t *src_pointer = (uint8_t *)src;
    rmt_item32_t *dest_pointer = dest;
    while (size < src_size && num < wanted_num) {
        for (int i = 0; i < 8; i++) {
            auto rmt_item32 = create_rmt_item32(*src_pointer & (1 << (7 - i)));
            dest_pointer->val = rmt_item32.val;
            num++;
            dest_pointer++;
        }
        size++;
        src_pointer++;
    }
    *translated_size = size;
    *item_num = num;
}

SK6812::SK6812(gpio_num_t pin, size_t led_count, rmt_channel_t channel, size_t color_size)
    : pin(pin),
      led_count(led_count),
      channel(channel),
      buffer(new uint8_t[led_count * color_size]),
      color_size(color_size) {
    ESP_LOGI(TAG, "Using %d bytes for RMT buffer.", led_count * color_size);
}

void SK6812::init() {
    rmt_config_t config = RMT_DEFAULT_CONFIG_TX(pin, channel);
    config.clk_div = 1;

    ESP_LOGI(TAG, "Installing SK6812 RMT driver...");
    ESP_ERROR_CHECK(rmt_config(&config));
    ESP_ERROR_CHECK(rmt_driver_install(channel, 0, ESP_INTR_FLAG_LEVEL3));
    rmt_translator_init(config.channel, rmt_translator);

    uint32_t rmt_clock_speed_hz = 0;
    rmt_get_counter_clock(config.channel, &rmt_clock_speed_hz);
    uint32_t tick_length_ns = 1000000000 / rmt_clock_speed_hz;

    ticks_t0h = SK6812_T0H_NS / tick_length_ns;
    ticks_t0l = SK6812_T0L_NS / tick_length_ns;
    ticks_t1h = SK6812_T1H_NS / tick_length_ns;
    ticks_t1l = SK6812_T1L_NS / tick_length_ns;

    ESP_LOGI(
        TAG,
        "Calculated SK6812 clock timings: T0H = %d, T0L = %d, T1H = %d, T1L = %d.",
        ticks_t0h,
        ticks_t0l,
        ticks_t1h,
        ticks_t1l);
}

void SK6812::reset(size_t led_count) {
    ESP_LOGI(TAG, "Resetting from %d LEDs to %d LEDs (=%d bytes).", this->led_count, led_count, led_count * color_size);
    delete buffer;
    this->led_count = led_count;
    buffer = new uint8_t[led_count * color_size];
}

SK6812::SK6812(const SK6812 &sk6812) : SK6812(sk6812.pin, sk6812.led_count, sk6812.channel, sk6812.color_size) {
    memcpy(buffer, sk6812.buffer, led_count * color_size);
}

SK6812::SK6812(SK6812 &&sk6812) noexcept
    : SK6812(
          std::exchange(sk6812.pin, GPIO_NUM_0),
          std::exchange(sk6812.led_count, 0),
          std::exchange(sk6812.channel, RMT_CHANNEL_0),
          std::exchange(sk6812.color_size, 0)) {
    delete buffer;
    this->buffer = sk6812.buffer;
    sk6812.buffer = nullptr;
}

void SK6812::refresh() {
    ESP_ERROR_CHECK(rmt_write_sample(channel, buffer, led_count * color_size, true));
    rmt_wait_tx_done(channel, pdMS_TO_TICKS(2000));
}

void SK6812::set_color(size_t i, Color *color) {
    uint8_t *buffer_pointer = buffer;
    memcpy(buffer_pointer + i * color_size, reinterpret_cast<void *>(color->get_grbw().array.data()), color_size);
}

SK6812::~SK6812() {
    delete buffer;
}