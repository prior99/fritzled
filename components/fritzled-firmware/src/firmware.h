#ifndef __FIRMWARE_H
#define __FIRMWARE_H

#include "sk6812.h"
#include <cstdint>
#include <grid.h>
#include <scenes.h>
#include <setup.h>
#include <json11.hpp>
#include <mqtt_client.h>

class Firmware {
  public:
    Firmware(Setup &&setup, SK6812 sk6812);
    ~Firmware() = default;
    void init();
    void main_loop(uint32_t delta_t_ms);
    void on_standby();
    void on_previous_scene();
    void on_next_scene();
    void show_color(uint8_t r, uint8_t g, uint8_t b, uint8_t w, uint32_t time_ms);
    uint32_t get_scene_index();
    const std::string get_scene_name();
    void set_scene(const std::string &name);
    Setup *get_setup();
    Grid *get_grid();
    void render();
    json11::Json to_json();
    void change_setup(uint32_t boxesX, uint32_t boxesY, uint32_t bottlesX, uint32_t bottlesY);
    void test_images();
    void set_mqtt(esp_mqtt_client_handle_t mqtt_client);
    void publish_to_mqtt();

    bool running = false;

  private:
    void refresh_scene();

    Setup setup;
    Grid grid;
    SK6812 sk6812;
    std::unique_ptr<Scene> scene;
    uint32_t scene_index = 0;
    esp_mqtt_client_handle_t mqtt_client = nullptr;
};

#endif