#include <esp_event.h>
#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include "mqtt.h"

#include "persistance.h"
#include "firmware.h"
#include "hardware.h"
#include "main.h"
#include "sk6812.h"
#include "wifi.h"
#include <fritz-config.h>
#include <grid.h>
#include <setup.h>
#include <esp_log.h>

#define BIT_SUCCESS BIT0
#define BIT_FAILED BIT1

static EventGroupHandle_t main_loop_event_group;
static portMUX_TYPE mutex = portMUX_INITIALIZER_UNLOCKED;
static const char* TAG = "main";


void main_loop(void *args) {
    Firmware *firmware = reinterpret_cast<Firmware *>(args);
    auto last_tick = xTaskGetTickCount();
    ESP_LOGI(TAG, "Initializing firmware...");
    firmware->init();
    while (true) {
        auto current_tick = xTaskGetTickCount();
        auto diff_ms = (current_tick - last_tick) * portTICK_RATE_MS;
        last_tick = current_tick;
        if (firmware->running) {
            firmware->main_loop(diff_ms);
            vTaskDelay(50 / portTICK_PERIOD_MS);
        } else {
            vTaskDelay(1000 / portTICK_PERIOD_MS);
        }
    }
}

void app_main() {

    ESP_LOGI(TAG, "Initializing event loop...");
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    main_loop_event_group = xEventGroupCreate();

    ESP_LOGI(TAG, "Initializing persistance...");
    static Persistance persistance;
    persistance.init();

    ESP_LOGI(TAG, "Loading hardware setup from persistance...");
    Setup setup;
    persistance_error_t err = persistance.load_setup(setup);
    if (err == PERSISTANCE_ERROR) {
        ESP_LOGE(TAG, "Unable to load setup from persistance.");
    }
    if (err == PERSISTANCE_UNINITIALIZED) {
        ESP_LOGI(TAG, "Setup not initialized in persistance.");
    }
    static Firmware firmware(std::move(setup), SK6812(PIN_DATA_OUT, setup.bottles(), RMT_CHANNEL));

    ESP_LOGI(TAG, "Connecting to Wifi...");
    connect_wifi(&firmware, &persistance);

    auto mqtt_client = connect_mqtt(&firmware);
    firmware.set_mqtt(mqtt_client);

    ESP_LOGI(TAG, "Initializing GPIO...");
    setup_gpio(&firmware);
    indicate_running();

    ESP_LOGI(TAG, "Starting main loop...");
    TaskHandle_t xHandle = nullptr;
    xTaskCreatePinnedToCore(main_loop, "main_loop", 4096, &firmware, configMAX_PRIORITIES - 1, &xHandle, 1);
    configASSERT(xHandle);

    ESP_LOGI(TAG, "Waiting for main loop to exit...");
    EventBits_t bits = xEventGroupWaitBits(main_loop_event_group, BIT_SUCCESS | BIT_FAILED, false, false, portMAX_DELAY);
    if (bits & BIT_FAILED) {
        ESP_LOGE(TAG, "Main loop exited with failure.");
    } else {
        ESP_LOGE(TAG, "Main loop exited successfully.");
    }
}