#ifndef MQTT_H
#define MQTT_H

#include "firmware.h"
#include <mqtt_client.h>

struct mqtt_args_t {
    Firmware *firmware;
};

esp_mqtt_client_handle_t connect_mqtt(Firmware *firmware);

#endif
