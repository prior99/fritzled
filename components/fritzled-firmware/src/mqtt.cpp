#include "mqtt.h"
#include "wifi-config.h"
#include <esp_err.h>
#include <esp_log.h>

static const char *TAG = "mqtt";

static void mqtt_error(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGI(TAG, "MQTT Error.");
}

static void mqtt_disconnected(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGI(TAG, "MQTT disconnected.");
}

static void mqtt_subscribed(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGI(TAG, "MQTT subscribed.");
}

static void mqtt_published(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGI(TAG, "MQTT published.");
}

static void mqtt_connected(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    mqtt_args_t *args = reinterpret_cast<mqtt_args_t *>(handler_args);
    esp_mqtt_event_handle_t event = reinterpret_cast<esp_mqtt_event_handle_t>(event_data);
    esp_mqtt_client_handle_t client = event->client;
    ESP_LOGI(TAG, "MQTT connected.");
    esp_mqtt_client_subscribe(client, "home/fritzled/set", 0);
    args->firmware->publish_to_mqtt();
}

static void mqtt_data(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    using json11::Json;
    mqtt_args_t *args = reinterpret_cast<mqtt_args_t *>(handler_args);
    esp_mqtt_event_handle_t event = reinterpret_cast<esp_mqtt_event_handle_t>(event_data);
    esp_mqtt_client_handle_t client = event->client;

    std::string err;
    std::string data(event->data, event->data + event->data_len);

    ESP_LOGI(TAG, "MQTT data received: %s", data.c_str());

    Json json = Json::parse(data, err);

    if (json.is_null()) {
        ESP_LOGI(TAG, "Failed to parse json (%s): %s", err.c_str(), data.c_str());
    }


    if (json.is_object()) {
        auto state = json["state"];

        if (state.string_value() == "ON" && !args->firmware->running) {
            ESP_LOGI(TAG, "Nothing running, but desired state was 'ON'. Toggling standby.");
            args->firmware->on_standby();
        }
        if (state.string_value() == "OFF" && args->firmware->running) {
            ESP_LOGI(TAG, "Running, but desired state was 'OFF'. Toggling standby.");
            args->firmware->on_standby();
        }

        auto effect = json["effect"];
        if (effect.is_string()) {
            ESP_LOGI(TAG, "Request to change effect to: %s", effect.string_value().c_str());
            args->firmware->set_scene(effect.string_value());
        }
    } else {
        ESP_LOGE(TAG, "MQTT data was not an object: %s", data.c_str());
    }
}

esp_mqtt_client_handle_t connect_mqtt(Firmware *firmware) {
    static mqtt_args_t args{firmware};
    esp_mqtt_client_config_t mqtt_cfg = {
        .uri = MQTT_BROKER_URL,
        .client_id = "fritzled",
        .username = MQTT_USERNAME,
        .password = MQTT_PASSWORD,
    };
    esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(client, MQTT_EVENT_ERROR, mqtt_error, &args);
    esp_mqtt_client_register_event(client, MQTT_EVENT_DISCONNECTED, mqtt_disconnected, &args);
    esp_mqtt_client_register_event(client, MQTT_EVENT_SUBSCRIBED, mqtt_subscribed, &args);
    esp_mqtt_client_register_event(client, MQTT_EVENT_PUBLISHED, mqtt_published, &args);
    esp_mqtt_client_register_event(client, MQTT_EVENT_CONNECTED, mqtt_connected, &args);
    esp_mqtt_client_register_event(client, MQTT_EVENT_DATA, mqtt_data, &args);
    ESP_LOGI(TAG, "Connecting to MQTT...");
    esp_mqtt_client_start(client);
    return client;
}