#ifndef __SK6812_H
#define __SK6812_H
#include "esp_log.h"
#include <color.h>
#include <driver/gpio.h>
#include <driver/rmt.h>
#include <stdint.h>

#define SK6812_T0H_NS 300
#define SK6812_T0L_NS 900
#define SK6812_T1H_NS 600
#define SK6812_T1L_NS 600

class SK6812 {
  public:
    SK6812(gpio_num_t pin, size_t led_count, rmt_channel_t channel = RMT_CHANNEL_0, size_t color_size = 4);
    SK6812(const SK6812 &sk6812);
    SK6812(SK6812 &&sk6812) noexcept;
    void refresh();
    void set_color(size_t index, Color *color);
    virtual ~SK6812();
    void init();
    void reset(size_t led_count);

  private:
    gpio_num_t pin;
    size_t led_count;
    rmt_channel_t channel;
    uint8_t *buffer;
    size_t color_size;
};

#endif
