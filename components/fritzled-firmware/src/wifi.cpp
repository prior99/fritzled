#include "wifi.h"
#include "persistance.h"
#include <cmath>
#include <esp_event.h>
#include <esp_log.h>
#include <esp_wifi.h>
#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <freertos/task.h>
#include <functional>
#include <nvs_flash.h>
#include <scenes.h>
#include <string>

#define BIT_CONNECTED BIT0
#define BIT_FAILED BIT1

static EventGroupHandle_t wifi_event_group;
static esp_netif_t *netif = nullptr;
static uint32_t connect_attempts = 0;
static const char *TAG = "wifi";

static void on_started(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data) {
    ESP_ERROR_CHECK(esp_wifi_connect());
}

static void on_disconnect(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data) {
    connect_attempts++;
    if (connect_attempts > WIFI_MAX_RETRY) {
        xEventGroupSetBits(wifi_event_group, BIT_FAILED);
    } else {
        esp_wifi_connect();
    }
}

static void on_initial_connect(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data) {
    connect_attempts = 0;
    xEventGroupSetBits(wifi_event_group, BIT_CONNECTED);
}

static void on_connect(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data) {
    connect_attempts = 0;
}

void connect_wifi(Firmware *firmware, Persistance *persistance) {
    // Setup network interface.
    ESP_ERROR_CHECK(esp_netif_init());

    // Create event group for WIFI.
    wifi_event_group = xEventGroupCreate();

    // Setup wifi connection.
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    cfg.wifi_task_core_id = 0;
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    // Configure network interface.
    esp_netif_config_t netif_config = ESP_NETIF_DEFAULT_WIFI_STA();
    netif = esp_netif_new(&netif_config);
    ESP_LOGI(TAG, "Setting hostname to: \"%s\".", "fritzled");
    esp_netif_set_hostname(netif, "fritzled");
    esp_netif_attach_wifi_station(netif);
    esp_wifi_set_default_wifi_sta_handlers();

    // Attach handler for wifi initialized.
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_STA_START, &on_started, nullptr));

    // Initialize the connection on initial connect.
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &on_initial_connect, nullptr));

    // Handle disconnect.
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &on_disconnect, nullptr));

    // Configure WIFI.
    wifi_config_t wifi_config = {
        .sta = {WIFI_SSID, WIFI_PASSWORD},
    };
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    // Wait for connection to WIFI AP to be established.
    EventBits_t bits = xEventGroupWaitBits(wifi_event_group, BIT_CONNECTED | BIT_FAILED, false, false, portMAX_DELAY);

    // After the connection has been established, unregister the handlers.
    ESP_ERROR_CHECK(esp_event_handler_unregister(WIFI_EVENT, WIFI_EVENT_STA_START, &on_started));
    ESP_ERROR_CHECK(esp_event_handler_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, &on_initial_connect));

    // On reconnects, reset attempts.
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &on_connect, nullptr));

    // Bail on error.
    if (!(bits & BIT_CONNECTED)) {
        return;
    }
}