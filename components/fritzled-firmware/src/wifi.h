#ifndef __WIFI_H
#define __WIFI_H

#include <esp_http_server.h>
#include "firmware.h"
#include "wifi-config.h"
#include "persistance.h"

#define WIFI_MAX_RETRY 5

struct wifi_args_t {
    httpd_handle_t *server;
    Firmware *firmware;
    Persistance *persistance;
};

void connect_wifi(Firmware *firmware, Persistance *persistance);

#endif