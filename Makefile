TTY := $(if $(TTY),$(TTY),/dev/ttyUSB0)

.PHONY: default
default: all

.PHONY: all
all: firmware demo

.PHONY: submodules
submodules:
	git submodule update --init

.PHONY: firmware
firmware: venv submodules
	mkdir -p build-firmware
	cd build-firmware && source ../venv/bin/activate && cmake ..\
		-DPYTHON_EXECUTABLE=venv/bin/python\
		-DCMAKE_TOOLCHAIN_FILE=$(IDF_PATH)/tools/cmake/toolchain-esp32.cmake\
		-DTARGET=esp32\
		-GNinja
	source venv/bin/activate && cmake --build build-firmware

.PHONY: demo
demo: submodules
	mkdir -p build-demo
	cd build-demo && cmake .. -GNinja
	cmake --build build-demo

.PHONY: run
run: demo
	build-demo/components/fritzled-demo/fritzled-demo

.PHONY: flash
flash: venv firmware
	cd build-firmware/components/fritzled-firmware &&\
	../../../venv/bin/python $(IDF_PATH)/components/esptool_py/esptool/esptool.py\
		--baud 921600\
		-p $(TTY) write_flash\
		@flash_bootloader_args\
		@flash_partition_table_args\
		@flash_app_args

.PHONY: monitor
monitor: venv
	venv/bin/python $(IDF_PATH)/tools/idf_monitor.py -p $(TTY) build-firmware/components/fritzled-firmware/fritzled.elf

.PHONY: clean
clean:
	rm -rf build-firmware build-demo venv

venv:
	virtualenv -p python2.7 venv
	source venv/bin/activate && pip install -r $(IDF_PATH)/requirements.txt